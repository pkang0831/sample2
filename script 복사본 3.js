// Javascript Element Files for index.html, Steepbank Ore Quality Dashboard
// With interest of - Data Science end-to-end deployment
// Author: Paul Kang, Primary Extraction Data Scientist, pkang@suncor.com

// This code is a collection of js elements, to be implemented in a full product.

// This is sample edit comment

const d50_data = [200,250,150,100,150,180,100,80,200,300,100,400,100,200,150,180,140,130];
const d50_labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
const d50_data_new = [];
const d50_labels_new = [];
const d50_backgroundcolor = [];
const d50_limit = 220

for (i = 1; i < d50_data.length; i++) {
    if (d50_data[i-1] < d50_limit && d50_data[i] > d50_limit) {
        d50_data_new.push(d50_data[i-1])
        d50_labels_new.push(d50_labels[i-1])
        d50_data_new.push(d50_limit)
        slope = (d50_data[i] - d50_data[i-1])/(d50_labels[i] - d50_labels[i-1])
        intercept = (d50_data[i-1]-slope*d50_labels[i-1])
        new_pt = (d50_limit-intercept)/slope
        d50_labels_new.push(new_pt)
    } else if(d50_data[i-1] > d50_limit && d50_data[i] < d50_limit){
        d50_data_new.push(d50_data[i-1])
        d50_labels_new.push(d50_labels[i-1])
        d50_data_new.push(d50_limit)
        slope = (d50_data[i] - d50_data[i-1])/(d50_labels[i] - d50_labels[i-1])
        intercept = (d50_data[i-1]-slope*d50_labels[i-1])
        new_pt = (d50_limit-intercept)/slope
        d50_labels_new.push(new_pt)
    } else if(d50_data[i-1] < d50_limit && d50_data[i] < d50_limit){
        d50_data_new.push(d50_data[i-1])
        d50_labels_new.push(d50_labels[i-1])
    } else if(d50_data[i-1] > d50_limit && d50_data[i] > d50_limit) {
        d50_data_new.push(d50_data[i-1])
        d50_labels_new.push(d50_labels[i-1])
    }
}
d50_backgroundcolor.push('')
for(i = 1; i < d50_data_new.length; i++) {
    if(d50_data_new[i-1] < d50_limit && d50_data_new[i] == d50_limit) { 
        d50_backgroundcolor.push('green')
    } else if(d50_data_new[i-1] == d50_limit && d50_data_new[i] > d50_limit) { 
        d50_backgroundcolor.push('red')
    } else if(d50_data_new[i-1] > d50_limit && d50_data_new[i] == d50_limit) { 
        d50_backgroundcolor.push('red') 
    } else if(d50_data_new[i-1] == d50_limit && d50_data_new[i] < d50_limit) {
        d50_backgroundcolor.push('green')
    } else if(d50_data_new[i-1] > d50_limit && d50_data_new[i] > d50_limit) {
        d50_backgroundcolor.push('red') 
    } else if(d50_data_new[i-1] < d50_limit && d50_data_new[i] < d50_limit) {
        d50_backgroundcolor.push('green') 
    }
};

console.log(d50_data_new,d50_backgroundcolor)

var ctx = document.getElementById('myChartD50').getContext('2d');
//adding custom chart type
Chart.defaults.multicolorLine = Chart.defaults.line;
Chart.controllers.multicolorLine = Chart.controllers.line.extend({
  draw: function(ease) {
    var
      startIndex = 0,
      meta = this.getMeta(),
      points = meta.data || [],
      colors = this.getDataset().colors,
      area = this.chart.chartArea,
      originalDatasets = meta.dataset._children
        .filter(function(data) {
          return !isNaN(data._view.y);
        });

    function _setColor(newColor, meta) {
      meta.dataset._view.borderColor = newColor;
    }

    if (!colors) {
      Chart.controllers.line.prototype.draw.call(this, ease);
      return;
    }

    for (var i = 2; i <= colors.length; i++) {
      if (colors[i-1] !== colors[i]) {
        _setColor(colors[i-1], meta);
        meta.dataset._children = originalDatasets.slice(startIndex, i);
        meta.dataset.draw();
        startIndex = i - 1;
      }
    }

    meta.dataset._children = originalDatasets.slice(startIndex);
    meta.dataset.draw();
    meta.dataset._children = originalDatasets;

    points.forEach(function(point) {
      point.draw(area);
    });
  }
});

var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'multicolorLine',

    // The data for our dataset
    data: {
        labels: d50_labels_new,
        datasets: [{
            label: "D50, limit: 220 um",
            borderColor:'grey',
            data: d50_data_new,
            //first color is not important
            colors: d50_backgroundcolor,
            pointBackgroundColor:'grey',
            pointRadius: 4,
            borderWidth: 2,
            pointHoverRadius: 0,
            hoverBorderWidth:3,
            hoverBorderColor:'white'
        }]
    },

    // Configuration options go here
    options: {
        title:{
            display:false,
            text:'D50',
            fontSize:15
        },
        legend: {
            labels: {
                fontColor: "white",
                fontSize: 15
            }
        },
        scales:{
            yAxes: [{
                display:true,
                ticks: {
                    fontColor:'white'
                },
                scaleLabel: {
                    display:true,
                    labelString: 'D50',
                    fontColor:'white',
                    fontSize: 20
                },
                gridLines: {
                    display: false,
                    color:'grey'
                }
            }],
            xAxes: [{
                display:true,
                ticks: {
                    fontColor:'white'
                },
                scaleLabel: {
                    display:true,
                    labelString: 'Time',
                    fontColor:'white',
                    fontSize: 20
                },
                gridLines: {
                    display: true,
                    color:'grey'
                }
            }]
        }
    }
});















// var myChartD50 = document.getElementById('myChartD50').getContext('2d')

// var massPopchartD50 = new Chart(myChartD50, {
//     type:'line', //bar, horizontalBar,pie,line,doughnuts,radar,polararea
//     data:{
//         labels:['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18'],
//         datasets:[{
//             label:'D50',
//             fill: false,
//             data: d50_data,
//             borderWidth:2,
//             // borderColor: backgroundcolor,
//             // backgroundColor: backgroundcolor,
//             hoverBorderWidth:3,
//             hoverBorderColor:'#125',
//             segment: {
//                 borderColor: ctx => skipped(ctx, 'rgb(0,0,0,0.2)') || down(ctx, 'rgb(192,75,75)'),
//                 borderDash: ctx => skipped(ctx, [6, 6]),
//               }
//         },{
//             label:'Limit: 220 um',
//             fill: false,
//             data:[220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220],
//             borderWidth:2,
//             borderColor:'red',
//             backgroundColor:'red',
//             hoverBorderWidth:3,
//             hoverBorderColor:'#125',
//             borderDash: [8,4],
//             pointRadius: 0,
//             pointHoverRadius: 0
//         }],
//     },
//     options:{
        
//     }
// })



var myChartFines = document.getElementById('myChartFines').getContext('2d')

var massPopchartFines = new Chart(myChartFines, {
    type:'line', //bar, horizontalBar,pie,line,doughnuts,radar,polararea
    data:{
        labels:['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18'],
        datasets:[{
            label:'Fines',
            fill: false,
            data:[20,25,15,10,10,18,10,8,20,17,10,14,10,20,15,18,14,13],
            borderWidth:2,
            borderColor:'grey',
            backgroundColor:'white',
            hoverBorderWidth:3,
            hoverBorderColor:'#125'
        }],
    },
    options:{
        title:{
            display:false,
            text:'Fines',
            fontSize:15
        },
        legend: {
            labels: {
                fontColor: "white",
                fontSize: 15
            }
        },
        scales:{
            yAxes: [{
                display:true,
                ticks: {
                    fontColor:'white'
                }
            }],
            xAxes: [{
                display:true,
                ticks: {
                    fontColor:'white'
                }
            }]
        }
    }
})


var myChartMBI = document.getElementById('myChartMBI').getContext('2d')

var massPopchartMBI = new Chart(myChartMBI, {
    type:'line', //bar, horizontalBar,pie,line,doughnuts,radar,polararea
    data:{
        labels:['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18'],
        datasets:[{
            label:'MBI',
            fill: false,
            data:[200,250,150,100,150,180,100,80,200,300,100,400,100,200,150,180,140,130],
            borderWidth:2,
            borderColor:'grey',
            backgroundColor:'white',
            hoverBorderWidth:3,
            hoverBorderColor:'#125'
        }],
    },
    options:{
        title:{
            display:false,
            text:'MBI',
            fontSize:15
        },
        legend: {
            labels: {
                fontColor: "white",
                fontSize: 15
            }
        },
        scales:{
            yAxes: [{
                display:true,
                ticks: {
                    fontColor:'white'
                }
            }],
            xAxes: [{
                display:true,
                ticks: {
                    fontColor:'white'
                }
            }]
        }
    }
})


var myChartBit = document.getElementById('myChartBit').getContext('2d')

var massPopchartBit = new Chart(myChartBit, {
    type:'line', //bar, horizontalBar,pie,line,doughnuts,radar,polararea
    data:{
        labels:['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18'],
        datasets:[{
            label:'% Bitumen',
            fill: false,
            data:[200,250,150,100,150,180,100,80,200,300,100,400,100,200,150,180,140,130],
            borderWidth:2,
            borderColor:'grey',
            backgroundColor:'white',
            hoverBorderWidth:3,
            hoverBorderColor:'#125'
        }],
    },
    options:{
        title:{
            display:false,
            text:'Bitumen',
            fontSize:15
        },
        legend: {
            labels: {
                fontColor: "white",
                fontSize: 15
            }
        },
        scales:{
            yAxes: [{
                display:true,
                ticks: {
                    fontColor:'white'
                }
            }],
            xAxes: [{
                display:true,
                ticks: {
                    fontColor:'white'
                }
            }]
        }
    }
})


// var myChartFroth_to_Ore = document.getElementById('myChartFroth_to_Ore').getContext('2d')

// var massPopchartFroth_to_Ore = new Chart(myChartFroth_to_Ore, {
//     type:'bar', //bar, horizontalBar,pie,line,doughnuts,radar,polararea
//     data:{
//         labels:['1AM','2AM','3AM','4AM','5AM','6AM','7AM','8AM','9AM','10AM','11AM','12AM'],
//         datasets:[{
//             label:'Tonnage Production',
//             fill: true,
//             data:[8850,8845,9000,9124,9512,9847,10011,10103,10302,10597,10487,10489],
//             borderWidth:2,
//             borderColor:'#8B8000',
//             backgroundColor:'#8B8000',
//             hoverBorderWidth:3,
//             hoverBorderColor:'white'
//         },{
//             label:'Froth Production',
//             fill: true,
//             data: [8950,8945,9100,9224,9612,9947,11011,10303,10202,10697,10587,10589],
//             borderWidth:2,
//             borderColor:'teal',
//             backgroundColor:'teal',
//             hoverBorderWidth:3,
//             hoverBorderColor:'white'
//         }],
//     },
//     options:{
//         title:{
//             display:false,
//             text:'Production',
//             fontSize:15
//         },
//         legend: {
//             labels: {
//                 fontColor: "white",
//                 fontSize: 15
//             }
//         },
//         scales:{
//             yAxes: [{
//                 display:true,
//                 ticks: {
//                     fontColor:'white',
//                     suggestedMin:0,
//                     suggestedMax:11000
//                 },
//                 gridLines:{
//                     display:true,
//                     color:'grey'
//                 }
//             },{
//                 stacked:false
//             }],
//             xAxes: [{
//                 stacked:false,
//                 ticks: {
//                     fontColor:'white'
//                 }
//             }]
//         }
//     }
// })


// var myChartShovel_Contributors = document.getElementById('myChartShovel_Contributors').getContext('2d')

// var massPopchartShovel_Contributors = new Chart(myChartShovel_Contributors, {
//     type:'pie', //bar, horizontalBar,pie,line,doughnuts,radar,polararea
//     data:{
//         labels:['1610','1612','1613','1401','1402','1606'],
//         datasets:[{
//             label:'Oilsands Shovels %',
//             fill: true,
//             data:[8850,7435,3450,2030,1025,543],
//             borderWidth:2,
//             borderColor:'white',
//             backgroundColor:['rgba(50,130,190,255)',
//                             'rgba(80,150,200,255)',
//                             'rgba(110,170,210,255)',
//                             'rgba(140,190,220,255)',
//                             'rgba(170,210,230,255)',
//                             'rgba(200,240,240,255)'],
//             hoverBorderWidth:3,
//             hoverBorderColor:'white'
//         }],
//     },
//     options:{
//         title:{
//             display:false,
//             text:'Production',
//             fontSize:15
//         },
//         legend: {
//             labels: {
//                 fontColor: "white",
//                 fontSize: 15
//             }
//         },
//     }
// })

// var myChartShovel_Information = document.getElementById('myChartShovel_Information').getContext('2d')

// var massPopchartShovel_Information = new Chart(myChartShovel_Information, {
//     type:'radar', //bar, horizontalBar,pie,line,doughnuts,radar,polararea
//     data:{
//         labels:['% Fines','% Bitumen','MBI','D50'],
//         datasets:[{
//             label:'Oilsands Shovels %',
//             fill: true,
//             data:[16,12,200,175],
//             borderWidth:2,
//             borderColor:'white',
//             backgroundColor:'red',
//             hoverBorderWidth:3,
//             hoverBorderColor:'white'
//         }],
//     },
//     options:{
//         title:{
//             display:false,
//             text:'Shovel_%',
//             fontSize:15
//         },
//         legend: {
//             labels: {
//                 fontColor: "white",
//                 fontSize: 15
//             }
//         },
//     }
// })